package com.example.illiakozhara.googledatabindinglesson1;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.illiakozhara.googledatabindinglesson1.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        generateSomeUser(true);

    }

    /**
     * Some user blank generator
     * @param status boolean onLine status
     */
    private void generateSomeUser(Boolean status) {
        TestUser user = new TestUser("Test User", "test_user@email.com", status);
        binding.setUser(user);
    }
}
