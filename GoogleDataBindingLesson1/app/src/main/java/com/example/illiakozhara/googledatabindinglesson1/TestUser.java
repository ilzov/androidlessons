package com.example.illiakozhara.googledatabindinglesson1;

/**
 * Created by Illia Kozhara on 27.07.2017.
 */

public class TestUser {

    public final String name;
    public final String eMail;
    public Boolean status;

    public TestUser(String name, String eMail, Boolean status) {
        this.name = name;
        this.eMail = eMail;
        this.status = status;
    }
}
